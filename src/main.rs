fn main() {
    const COUNT: usize = 100;
    for num in 1..=COUNT {
        match (num % 3, num % 5) {
            (0, 0) => println!("FizzBuzz"),
            (0, _) => println!("Fizz"),
            (_, 0) => println!("Buzz"),
            (_, _) => println!("{num}")
        }
    }
}
